FROM eclipse-temurin:17-jdk-alpine AS BUILDTOOLS
ADD https://hub.spigotmc.org/jenkins/job/BuildTools/149/artifact/target/BuildTools.jar /BuildTools.jar
RUN apk --update add git
WORKDIR /root/.m2
RUN --mount=type=cache,target=/root/.m2 java -jar /BuildTools.jar --remapped --rev 1.19.2

FROM maven:3.8.3-eclipse-temurin-17 AS BUILD
COPY --from=BUILDTOOLS /root/.m2 /root/.m2
COPY ./ /home/app/
RUN --mount=type=cache,target=/root/.m2 mvn -f /home/app/pom.xml clean package

FROM eclipse-temurin:17-jdk-alpine AS SPIGOT
WORKDIR /root/spigot
ADD https://api.papermc.io/v2/projects/paper/versions/1.19.2/builds/307/downloads/paper-1.19.2-307.jar /paper-1.19.2.jar
COPY --from=BUILD /home/app/target/split-1.0.0-SNAPSHOT.jar /plugins/split-1.0.0-SNAPSHOT.jar
ADD https://ci.dmulloy2.net/job/ProtocolLib/606/artifact/target/ProtocolLib.jar /plugins/ProtocolLib.jar
ENTRYPOINT ["java", "-Dcom.mojang.eula.agree=true", "-jar", "/paper-1.19.2.jar", "nogui", "--plugins", "/plugins"]