package me.smourad.split;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;

public enum TeamColor {

    BLUE(ChatColor.BLUE, DyeColor.BLUE),
    RED(ChatColor.RED, DyeColor.RED);

    private final ChatColor chatColor;
    private final DyeColor dyeColor;

    TeamColor(ChatColor chatColor, DyeColor dyeColor) {
        this.chatColor = chatColor;
        this.dyeColor = dyeColor;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public DyeColor getDyeColor() {
        return dyeColor;
    }

}
