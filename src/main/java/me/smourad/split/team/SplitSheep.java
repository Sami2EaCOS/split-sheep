package me.smourad.split.team;

import lombok.Data;
import me.smourad.split.TeamColor;
import org.bukkit.*;
import org.bukkit.entity.*;

@Data
public class SplitSheep {

    private final Player passenger;
    private final Sheep vehicle;

    public SplitSheep(Location location, TeamColor team, Player passenger) {
        this.passenger = passenger;
        this.vehicle = setupVehicle(location, team);
    }

    private Sheep setupVehicle(Location location, TeamColor team) {
        World world = location.getWorld();
        Sheep sheep = (Sheep) world.spawnEntity(location, EntityType.SHEEP);
        sheep.setInvulnerable(true);
        sheep.setPersistent(true);
        sheep.setColor(team.getDyeColor());

        passenger.teleport(location);
        sheep.addPassenger(passenger);
        return sheep;
    }

    public void kill() {
        vehicle.setHealth(0);
        passenger.setGameMode(GameMode.SPECTATOR);
    }

}
