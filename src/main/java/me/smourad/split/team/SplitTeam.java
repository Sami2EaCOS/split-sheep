package me.smourad.split.team;

import me.smourad.split.TeamColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;

import java.util.List;
import java.util.stream.Stream;

public class SplitTeam {

    private final SplitSheep first;
    private final SplitSheep second;

    public SplitTeam(TeamColor team, Player first, Player second, Location loc1, Location loc2) {
        this.first = new SplitSheep(loc1, team, first);
        this.second = new SplitSheep(loc2, team, second);
    }

    public boolean containPlayer(Player player) {
        return Stream.of(first, second).anyMatch(split -> split.getPassenger().equals(player));
    }

    public Sheep getControlledVehicle(Player player) {
        return Stream.of(first, second)
                .filter(split -> !split.getPassenger().equals(player))
                .findFirst()
                .orElseThrow()
                .getVehicle();
    }

    public void kill() {
        Stream.of(first, second).forEach(SplitSheep::kill);
    }

    public List<Player> getPlayers() {
        return List.of(first.getPassenger(), second.getPassenger());
    }

}
