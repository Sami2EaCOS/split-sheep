package me.smourad.split;

import lombok.Getter;
import me.smourad.split.attack.TheBeamWizard;
import me.smourad.split.health.TheFairy;
import me.smourad.split.team.TeamObserver;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public final class SplitPlugin extends JavaPlugin {

    public static SplitPlugin INSTANCE; { INSTANCE = this; }

    @Getter
    private TeamObserver teamObserver;
    @Getter
    private TheBeamWizard theBeamWizard;
    @Getter
    private TheFairy theFairy;

    @Override
    public void onEnable() {
        teamObserver = new TeamObserver();
        theBeamWizard = new TheBeamWizard();
        theFairy = new TheFairy();

        List.of(teamObserver, theBeamWizard, theFairy)
                .forEach(listener ->  Bukkit.getPluginManager().registerEvents(listener, this));
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }



}
