package me.smourad.split.movement;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import me.smourad.split.SplitPlugin;
import me.smourad.split.TeamColor;
import me.smourad.split.team.SplitTeam;
import me.smourad.split.team.TeamObserver;
import org.bukkit.Location;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.util.Vector;
import org.spigotmc.event.entity.EntityDismountEvent;

import java.util.Objects;

public class MoveListener implements Listener {

    private final TeamObserver teamObserver;

    public MoveListener(TeamObserver teamObserver) {
        this.teamObserver = teamObserver;
        initMovePacketListener();
    }

    private void initMovePacketListener() {
        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(
                SplitPlugin.INSTANCE,
                ListenerPriority.NORMAL,
                PacketType.Play.Client.STEER_VEHICLE
        ) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                Player player = event.getPlayer();
                TeamColor color = teamObserver.getPlayers().get(player);
                SplitTeam team = teamObserver.getTeams().get(color);

                if (Objects.nonNull(team)) {
                    Sheep vehicle = team.getControlledVehicle(player);
                    move(vehicle, player, event.getPacket());
                }
            }
        });
    }

    private void move(Sheep vehicle, Player player, PacketContainer packet) {
        Location location = player.getEyeLocation();
        Vector direction = location.getDirection();
        Vector ws = direction.clone().setY(0);
        Vector ad = ws.clone().crossProduct(new Vector(0,-1,0));

        ws.multiply(packet.getFloat().read(1));
        ad.multiply(packet.getFloat().read(0));

        double y = vehicle.getVelocity().getY();

        if (vehicle.isOnGround() && Boolean.TRUE.equals(packet.getBooleans().read(0))) {
            y = 0.5;
        }

        double speed = vehicle.isOnGround() ? 0.4 : 0.3;

        Vector velocity = ws.clone().add(ad);
        velocity.normalize().multiply(speed).setY(y);
        vehicle.setRotation(location.getYaw(), location.getPitch());
        vehicle.setVelocity(velocity);
    }

    @EventHandler
    public void onDismount(EntityDismountEvent event) {
        if (event.getEntity() instanceof Player) {
            event.setCancelled(true);
        }
    }

}
