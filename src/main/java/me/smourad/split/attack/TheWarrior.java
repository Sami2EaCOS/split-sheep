package me.smourad.split.attack;

import me.smourad.split.SplitPlugin;
import me.smourad.split.TeamColor;
import me.smourad.split.team.TeamObserver;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Objects;

public class TheWarrior implements Listener {


    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        AttributeInstance genericAttackSpeedAttribute = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED);
        if (genericAttackSpeedAttribute != null) genericAttackSpeedAttribute.setBaseValue(1024.0D);
    }

    @EventHandler
    public void onSwordAttack(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player player && event.getEntity() instanceof Player damaged) {
            ItemStack sword = player.getEquipment().getItemInMainHand();

            if (Material.GOLDEN_SWORD.equals(sword.getType())) {
                event.setDamage(6);

                World world = damaged.getWorld();
                world.spawnParticle(Particle.REDSTONE, damaged.getLocation(), 3,
                        1, 1, 1,
                        new Particle.DustOptions(Color.RED, 1)
                );

                if (damaged.getHealth() <= event.getFinalDamage()) {
                    TeamObserver teamObserver = SplitPlugin.INSTANCE.getTeamObserver();
                    TeamColor color = teamObserver.getPlayers().get(damaged);
                    teamObserver.kill(color, player);
                    event.setDamage(0);
                }
            }
        } else {
            event.setCancelled(true);
        }
    }


}
