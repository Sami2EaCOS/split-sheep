package me.smourad.split.attack;

import me.smourad.split.SplitPlugin;
import me.smourad.split.TeamColor;
import me.smourad.split.team.SplitTeam;
import me.smourad.split.team.TeamObserver;
import net.kyori.adventure.text.Component;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.RayTraceResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TheBeamWizard implements Listener {

    private final Map<LightningStrike, Player> strikes = new HashMap<>();

    @EventHandler
    public void channel(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack item = event.getItem();

        if (Objects.nonNull(item) && item.getType().equals(Material.BLAZE_ROD) && player.getCooldown(Material.BLAZE_ROD) <= 0) {
            World world = player.getWorld();
            Location eyeLocation = player.getEyeLocation();
            RayTraceResult rayTraceResult = world.rayTrace(
                    eyeLocation, eyeLocation.getDirection(),
                    30, FluidCollisionMode.ALWAYS, Boolean.TRUE, 1,
                    entity -> !Objects.equals(player, entity)
            );

            if (Objects.nonNull(rayTraceResult)) {
                Location target = rayTraceResult.getHitPosition().toLocation(world);
                strike(target, player);
                player.setCooldown(Material.BLAZE_ROD, 30);
            }
        }
    }

    @EventHandler
    public void onStrikeHit(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof LightningStrike strike && strikes.containsKey(strike)) {
            Player player = strikes.remove(strike);

            if (event.getEntity() instanceof Player damaged) {
                event.setDamage(4);

                if (damaged.getHealth() <= event.getFinalDamage()) {
                    TeamObserver teamObserver = SplitPlugin.INSTANCE.getTeamObserver();
                    TeamColor color = teamObserver.getPlayers().get(damaged);
                    teamObserver.kill(color, player);
                    event.setDamage(0);
                }
            }
        } else {
            event.setCancelled(true);
        }
    }

    public void strike(Location target, Player caster) {
        World world = target.getWorld();

        new BukkitRunnable() {
            private int ticks = 0;

            @Override
            public void run() {
                int seconds = ticks/20;

                if (seconds >= 2) {
                    LightningStrike strike = world.strikeLightning(target);
                    strikes.put(strike, caster);
                    cancel();
                } else {
                    world.spawnParticle(Particle.REDSTONE, target, 20,
                            2, 2, 2,
                            new Particle.DustOptions(Color.YELLOW, 1)
                    );
                }

                ticks += 1;
            }
        }.runTaskTimer(SplitPlugin.INSTANCE, 0, 1L);
    }

}
